'use strict';

const app = require('../../app.js');
const chai = require('chai');
const expect = chai.expect;

describe('Digital Human Lex Tests', function () {
  var env;

  before(() => {
    env = process.env;
    process.env = { KENDRA_INDEX: '0a8480aa-ff6d-4230-8d4b-3e86044b6e80' };
  });

  it('specific query test', function () {
    const event = require('../../../events/specificQueryEvent.json');
    const context = {};
    const expectedResponse =
      '- Infection\n- Musculoskeletal pain\n- Reflux\n- Aortic dissection\n' +
      '- MI\n- PE\n- Pneumothorax\n- Tamponade\n\nFor additional information: \n- https://www.adhb.health.nz/';

    const callback = (err, message) => {
      expect(message.dialogAction.message.content).to.equal(expectedResponse);
    };
    app.lambdaHandler(event, context, callback);
  });

  it('specialist query test', function () {
    const event = require('../../../events/specialistQueryEvent.json');
    const context = {};
    const expectedResponse =
      'Before calling a radiologist specialist please check you have the following: ' +
      '\n1. Patient details\n2. Medical History Form on Concerto\n 3. Type of Imaging required (CT, Ultrasound, MR, etc.)' +
      '\nALL REQUESTS FOR ARTERIAL IMAGING should be discussed with Vascular Surgery and Interventional Radiology to avoid performing expensive, unnecessary or inappropriate investigations' +
      '\nPhone: 93 5573';

    const callback = (err, message) => {
      expect(message.dialogAction.message.content).to.equal(expectedResponse);
    };
    app.lambdaHandler(event, context, callback);
  });

  it('fallback query test', function () {
    const event = require('../../../events/kendraFallbackQueryEvent.json');
    const context = {};
    const expectedResponse =
      'Here are some documents for you to review:\n' +
      '- https://s3.ap-southeast-2.amazonaws.com/digital-human-rmo-handbook/Chest Pain.pdf\n';

    const callback = (err, message) => {
      expect(message.dialogAction.message.content).to.equal(expectedResponse);
    };
    app.lambdaHandler(event, context, callback);
  });
});
