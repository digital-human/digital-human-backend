const { Kendra } = require('@aws-sdk/client-kendra');
const client = new Kendra({ region: 'ap-southeast-2' });

//------------------------ Intents ------------------------

// Function that handles queries that are specific to a column
// from a table from the common ward calls chapter.
async function specificQueryHandler(intentRequest, callback) {
  const wardCall = intentRequest.currentIntent.slots.WardCall;
  const queryType = intentRequest.currentIntent.slots.SpecificQueryType;

  //Checks for optional queryType slot in query to determine type
  //of query response (specific or general).
  if (queryType) {
    const params = {
      IndexId: process.env.KENDRA_INDEX,
      QueryText: `${queryType} ${wardCall}`,
      QueryResultTypeFilter: 'QUESTION_ANSWER',
    };

    try {
      const response = await client.query(params);
      const faqAnswerText = response.ResultItems[0].DocumentExcerpt.Text;

      let lexResponse = '';
      faqAnswerText.split(/\s*,\s*/).forEach((listItem) => {
        lexResponse += `- ${listItem}\n`;
      });

      lexResponse +=
        '\nFor additional information: \n- ' +
        response.ResultItems[0].DocumentURI;

      callback(
        close('Fulfilled', {
          contentType: 'CustomPayload',
          content: lexResponse,
        })
      );
    } catch (error) {
      console.log(`Error: ${error}`);
    }
  } else {
    kendraFallbackHandler(intentRequest, callback);
  }
}

// Function that handles queries for what's required prior to
// calling a specialist.
async function specialistQueryHandler(intentRequest, callback) {
  const specialistCall =
    intentRequest.currentIntent.slots.SpecialistCall.toLowerCase();
  let specialistContent = '';

  switch (specialistCall) {
    case 'cardiologist': {
      specialistContent =
        'Before calling a cardiologist specialist please check you have the following: ' +
        '\n1. Patient details\n2. Medical History Form on Concerto\n3. Cardio-surgical ' +
        'and MDM notes where applicable\n4. Medications to be with-held (I guess stuff theyre alergic too?)' +
        '\n5. Investigative results where applicable (eg. CXRs/ECHOs/Coronary Angiograms/CT scans,etc.)' +
        '\nPhone: 93 6383';
      break;
    }
    case 'radiologist': {
      specialistContent =
        'Before calling a radiologist specialist please check you have the following: ' +
        '\n1. Patient details\n2. Medical History Form on Concerto\n 3. Type of Imaging required (CT, Ultrasound, MR, etc.)' +
        '\nALL REQUESTS FOR ARTERIAL IMAGING should be discussed with Vascular Surgery and Interventional Radiology to avoid performing expensive, unnecessary or inappropriate investigations' +
        '\nPhone: 93 5573';
      break;
    }
    default:
      specialistContent = `Sorry we do not have information for a ${specialistCall} specialist.`;
      break;
  }

  callback(
    close('Fulfilled', {
      contentType: 'CustomPayload',
      content: specialistContent,
    })
  );
}

// Function that handles general user search queries.
async function kendraFallbackHandler(intentRequest, callback) {
  const params = {
    IndexId: process.env.KENDRA_INDEX,
    QueryText: intentRequest.inputTranscript,
    QueryResultTypeFilter: 'DOCUMENT',
  };

  try {
    const response = await client.query(params);
    let lexResponse = 'Sorry, I was not able to understand your query.';

    //Check for valid search results.
    if (response.ResultItems.length > 0) {
      lexResponse = 'Here are some documents for you to review:\n';

      let validFiles = 0;
      response.ResultItems.forEach((result) => {
        //FAQ documents (.csv) are not included for review.
        if (!result.DocumentId.endsWith('.csv')) {
          validFiles++;
          lexResponse += `- ${result.DocumentURI}\n`;
        }
      });

      if (validFiles == 0) {
        lexResponse = 'Sorry, there were no relevant documents to your query.';
      }
    }

    callback(
      close('Fulfilled', {
        contentType: 'CustomPayload',
        content: lexResponse,
      })
    );
  } catch (error) {
    console.log(error);
  }
}

//------------------------ Helpers ------------------------

// Helper function that handles close type response.
function close(fulfillmentState, message) {
  return {
    dialogAction: {
      type: 'Close',
      fulfillmentState: fulfillmentState,
      message: message,
    },
  };
}

//------------------------ Events ------------------------

// Function used to route query to appropriate intent handler.
function dispatch(intentRequest, callback) {
  const intentName = intentRequest.currentIntent.name;
  switch (intentName) {
    case 'SpecificQuery':
      specificQueryHandler(intentRequest, callback);
      break;

    case 'SpecialistQuery':
      specialistQueryHandler(intentRequest, callback);
      break;

    case 'Fallback':
      kendraFallbackHandler(intentRequest, callback);
      break;

    default:
      console.log('Unrecognised intent.');
      break;
  }
}

//--------------------- Main Handler ---------------------

exports.lambdaHandler = function (event, context, callback) {
  try {
    dispatch(event, (response) => {
      callback(null, response);
    });
  } catch (err) {
    console.log(err);
  }
};
