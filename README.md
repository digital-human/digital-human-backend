# Digital Human (Backend repo)

## Using the SAM CLI to build and deploy to AWS

**THIS IS DONE AUTOMATICALLY WHEN CODE IS PUSHED TO THE MAIN BRANCH**

Build your application with the `sam build` command.

```bash
digital-human-backend$ sam build
```

Deploy your application with the `sam deploy` command

```bash
digital-human-backend$ sam deploy
```

## Using the SAM CLI to build and test locally

Build your application with the `sam build` command.

```bash
digital-human-backend$ sam build
```

Test a single function by invoking it directly with a test event. An event is a JSON document that represents the input that the function receives from the event source. Test events are included in the `events` folder in this project.

Run functions locally and invoke them with the `sam local invoke` command.

```bash
digital-human-backend$ sam local invoke DigitalHumanLexFunction --event events/event.json
```

## Unit tests

Tests are defined in the `hello-world/tests` folder in this project. Use NPM to install the [Mocha test framework](https://mochajs.org/) and run unit tests from your local machine.

```bash
digital-human-backend$ cd hello-world
hello-world$ npm install
hello-world$ npm run test
```

## Cleanup

To delete the sample application that you created, use the AWS CLI. Assuming you used your project name for the stack name, you can run the following:

```bash
aws cloudformation delete-stack --stack-name digital-human-backend
```