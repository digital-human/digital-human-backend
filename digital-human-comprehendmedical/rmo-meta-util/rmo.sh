#!/bin/bash

FILES="Data/*"
for f in $FILES
do
  echo "Processing $f..."
  node ./index.js "$f" > "./Meta/$f.metadata.json"
done