const {
  ELIMIT,
  MIN_SCORE,
  COMPREHEND_MEDICAL_TEXT_SIZE,
  CATEGORIES,
} = require("./constants.js");
const { readFileSync } = require("fs");
const {
  ComprehendMedicalClient,
  DetectEntitiesV2Command,
} = require("@aws-sdk/client-comprehendmedical");

// Initialise the Comprehend Medical client
const client = new ComprehendMedicalClient({
  region: "ap-southeast-2",
  maxAttempts: "3",
});

/**
 * openDocument takes a string documentFilename as input and returns the opened file.
 * @param {string} documentFilename
 * @returns document
 */
function openDocument(documentFilename) {
  try {
    return readFileSync(documentFilename, "utf8");
  } catch (e) {
    console.log("Error:", e.stack);
  }
}

/**
 * splitDocument breaks a txt file down into blocks under Comprehend Medical's MAX_SIZE.
 * Uses a "pointer" to break along a sentence boundary, where possible.
 *
 * @param {string} document
 * @returns {string[]} textBlocks
 */
function splitDocument(document) {
  let index = 0;
  let textString = "";
  const textBlocks = [];

  while (index < document.length) {
    // If file size > COMPREHEND_MEDICAL_TEXT_SIZE, attempt to break at sentence boundary
    const fullStopIndex = document
      .substring(index + COMPREHEND_MEDICAL_TEXT_SIZE + 1)
      .search("\\.");
    if (fullStopIndex > 0 && fullStopIndex < 500) {
      // Found sentence boundary at fullStopIndex offset
      textString = document.substring(
        index,
        index + COMPREHEND_MEDICAL_TEXT_SIZE + fullStopIndex + 2
      );
      index += COMPREHEND_MEDICAL_TEXT_SIZE + fullStopIndex + 3;
    } else {
      // Encountered sentence boundary - substring smaller than COMPREHEND_MEDICAL_TEXT_SIZE or fullstop not found in next 500 chars.
      textString = document.substring(
        index,
        index + COMPREHEND_MEDICAL_TEXT_SIZE
      );
      index += COMPREHEND_MEDICAL_TEXT_SIZE;
    }
    textBlocks.push(textString);
  }
  return textBlocks;
}

/**
 * Asynchronous calls to Comprehend Medical allow for parallel processing of text blocks.
 * The response variable used to call processEntities() consists of an array of
 * DetectEntitiesV2CommandOutput objects
 *
 * @param {string[]} textBlocks
 */
async function detectEntities(textBlocks) {
  var responses = await Promise.all(
    textBlocks.map(async (textString) => {
      const params = { Text: textString };
      const command = new DetectEntitiesV2Command(params);

      try {
        return await client.send(command);
      } catch (error) {
        const { requestId, cfId, extendedRequestId } = error.$metadata;
        console.log({ error, requestId, cfId, extendedRequestId });
      }
    })
  );
  return responses;
}

/**
 *
 * @param {DetectEntitiesV2CommandOutput[]} responses
 */
function processEntities(responses) {
  let entityData = {}; // List of JSON objects to store entities
  let categoryText = {}; // List of observed text strings recognized as categories
  let textFrequency = {}; // Frequency of each text string

  // Sets up Object data structures to play nice when output to JSON
  for (const cat of CATEGORIES) {
    entityData[cat] = [];
    categoryText[cat] = [];
    textFrequency[cat] = {};
  }

  for (const data of responses) {
    for (const e of data.Entities) {
      /*
      For each recognized entity, take those that:
      1. Have a confidence score greater than MIN_SCORE
      2. Don't contain quotes
      3. Are previously unseen
      */
      if (
        e.Score > MIN_SCORE &&
        !e.Text.includes('"') &&
        !categoryText[e.Category].includes(e.Text.toUpperCase())
      ) {
        // Make a custom Kendra Attribute by assigning text to entity data
        entityData[e.Category].push(e.Text);
        // Track text in uppercase so treatment remains same regardless of case
        categoryText[e.Category].push(e.Text.toUpperCase());
        // Track frequency to determine highest occuring entities (Initial)
        textFrequency[e.Category][e.Text.toUpperCase()] = 1;
      } else if (categoryText[e.Category].includes(e.Text.toUpperCase())) {
        // Track frequency to determine highest occuring entities (Increment)
        textFrequency[e.Category][e.Text.toUpperCase()] += 1;
      }
    }
  }
  return { entityData, textFrequency };
}

/**
 * Take the listed top ELIMIT entities and output them as Attributes/Metadata
 * @param {{}} entityData
 * @param {{}} textFrequency
 */
function generateMeta(entityData, textFrequency, documentFilename) {
  const metadata = {};
  const attributes = {};

  // sort <K,V> pairs from high-low values (most frequent - least freq occuring) and take up to ELIMIT
  for (const cat of CATEGORIES) {
    metadata[cat] = []; // Sets up Object to play nice when output to JSON
    const topEntities = Object.entries(textFrequency[cat])
      .sort((a, b) => b[1] - a[1])
      .slice(0, ELIMIT);

    for (const e of topEntities) {
      const entity = e[0];
      for (const data of entityData[cat]) {
        if (data.toUpperCase() === entity) {
          metadata[cat].push(data);
        }
      }
    }
  }

  try {
    // use documentFilename to give a source_uri attribute.
    const path =
      "https://digital-human-rmo-handbook.s3.ap-southeast-2.amazonaws.com/Data/" +
      documentFilename;
    metadata["_source_uri"] = path;
    attributes["Attributes"] = metadata;
    return attributes;
  } catch (ex) {
    console.log(ex.stack);
  }
}

/**
 * printAttributes takes a JS string array, converts to JSON string, prints to stdout.
 * @param {string[]} attributes
 */
function printAttributes(attributes) {
  console.log(JSON.stringify(attributes, null, 4)); // Final Output - redirected via bash to make metadata file.
}

module.exports = {
  openDocument,
  splitDocument,
  detectEntities,
  processEntities,
  generateMeta,
  printAttributes,
};
