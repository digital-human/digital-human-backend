const { splitDocument } = require("../meta-util.js");
const { COMPREHEND_MEDICAL_TEXT_SIZE } = require("../constants");

describe("the string splitter", () => {
  it("should return an empty array for an empty document.", () => {
    const emptyDoc = "";

    const res = splitDocument(emptyDoc);

    expect(res).toStrictEqual([]);
  });

  it("returns a single array element if string size is less than to COMPREHEND_MEDICAL_TEXT_SIZE", () => {
    const string19k = "a";

    const res = splitDocument(string19k);

    expect(res.length).toBe(1);
    expect(res).toStrictEqual(["a"]);
  });

  it("returns a single array element if string size is equal to COMPREHEND_MEDICAL_TEXT_SIZE", () => {
    const string19k = "a".repeat(COMPREHEND_MEDICAL_TEXT_SIZE);

    const res = splitDocument(string19k);

    expect(res.length).toBe(1);
  });

  describe("when there is no fullstop, if the document length is longer than COMPREHEND_MEDICAL_TEXT_SIZE... ", () => {
    it("splits the file, returns two elements at the appropriate string length", () => {
      const str19kplus1 = "a".repeat(COMPREHEND_MEDICAL_TEXT_SIZE + 1);

      const res = splitDocument(str19kplus1);

      expect(res.length).toBe(2);
      expect(res[1]).toStrictEqual("a");
    });
    it("splits the file, returns three elements at the appropriate string length", () => {
      const str38k = "a".repeat(COMPREHEND_MEDICAL_TEXT_SIZE * 2);
      const str38kplus1 = "a".repeat(str38k.length + 1);

      const res = splitDocument(str38k);
      const res2 = splitDocument(str38kplus1);

      expect(res.length).toBe(2);
      expect(res2.length).toBe(3);
    });
  });

  describe(", when a sentence boundary(fullstop) is present...", () => {
    it("do not split, stay as one element when equal or less than COMPREHEND_MEDICAL_TEXT_SIZE", () => {
      const str19k = "a".repeat(COMPREHEND_MEDICAL_TEXT_SIZE - 2) + ".a";
      expect(str19k.length).toBe(COMPREHEND_MEDICAL_TEXT_SIZE);

      const res = splitDocument(str19k);

      expect(res.length).toBe(1);
    });

    it("split along the fullstop boundary if a fullstop is up to 500 chars past COMPREHEND_MEDICAL_TEXT_SIZE", () => {
      const str19500 =
        "a".repeat(COMPREHEND_MEDICAL_TEXT_SIZE + 249) + "." + "a".repeat(250);
      expect(str19500.length).toBe(COMPREHEND_MEDICAL_TEXT_SIZE + 500);

      const res = splitDocument(str19500);

      expect(res.length).toBe(2);
      expect(res[0].length).toBe(COMPREHEND_MEDICAL_TEXT_SIZE + 250) 
      expect(res[0].charAt(res[0].length - 1)).toBe(".");
    });

    it("split along COMPREHEND_MEDICAL_TEXT_SIZE if a fullstop is more than 500 chars past", () => {
      const str20k = ("a".repeat(COMPREHEND_MEDICAL_TEXT_SIZE + 599) +  "." + ("a".repeat(400)));
      expect(str20k.length).toBe(20000);

      const res = splitDocument(str20k);

      expect(res.length).toBe(2);
      expect(res[0].length).toBe(COMPREHEND_MEDICAL_TEXT_SIZE);
    });
  });
});
