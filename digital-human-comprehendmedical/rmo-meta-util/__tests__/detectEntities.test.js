const { detectEntities } = require("../meta-util.js");
const { mockClient } = require("aws-sdk-client-mock");
const {
  ComprehendMedicalClient,
  DetectEntitiesV2Command,
} = require("@aws-sdk/client-comprehendmedical");

const cmMock = mockClient(ComprehendMedicalClient);

beforeEach(() => {
  cmMock.reset();
});

describe("the DetectEntities Command...", () => {
  it("Should return a response promise per text chunk entered", async () => {
    const textBlocks = [["adrenal medulla"], ["lungs"], ["adrenaline"]];
    cmMock.on(DetectEntitiesV2Command).resolves({
      Entities: [
        { Category: "ANATOMY", id: "1", Text: "adrenal medulla" },
        { Category: "ANATOMY", id: "2", Text: "lungs" },
        { Category: "MEDICATION", id: "3", Text: "Adrenaline" },
      ],
    });

    const responses = await detectEntities(textBlocks);

    expect(responses.length).toBe(textBlocks.length);
  });
  it("Should throw an error when empty data is passed in", async () => {
    try {
      await detectEntities();
    } catch (e) {
      expect(e.toString()).toMatch("Error");
    }
    expect.assertions(1);
  });
  it("Should throw an error when client.send() exception thrown", async () => {
    const textBlocks = [["adrenal medulla"]];

    cmMock.on(DetectEntitiesV2Command).rejects({
      $metadata: {
        requestId: "X01",
        cfId: "X01",
        extendedRequestId: "X01",
      },
    });

    try {
      const params = { Text: textBlocks[0] };
      const command = new DetectEntitiesV2Command(params);
      await cmMock.send(command);
    } catch (e) {
      expect(e).toMatchObject({
        $metadata: {
          requestId: "X01",
          cfId: "X01",
          extendedRequestId: "X01",
        },
      });
    }
    expect.assertions(1);
  });
});
