const { generateMeta } = require("../meta-util.js");
const { CATEGORIES, ELIMIT } = require("../constants.js");
const testEntityData = require("./testEntityData.json");
const testTextFrequency = require("./testTextFrequency.json");
const testAttributes = require("./testAttributes.json");

describe("The meta generator should...", () => {
  it('should begin with "Attributes" as the root', () => {
    testFilename = "testeroo";

    const res = generateMeta(testEntityData, testTextFrequency, testFilename);

    expect.assertions(1);
    expect(JSON.stringify(res)).toMatch('{"Attributes":{');
  });

  it("append the source_URI as a hardcoded link to the S3 bucket", () => {
    const testFilename = "testeroo";

    const res = generateMeta(testEntityData, testTextFrequency, testFilename);

    expect.assertions(1);
    expect(res.Attributes._source_uri).toMatch(testFilename);
  });

  it("throw an exception when called with empty entityData or textFrequency", () => {
    const testFilename = "testeroo";

    expect.assertions(1);
    expect(() => {
      generateMeta("", "", testFilename);
    }).toThrow("Cannot convert undefined or null to object");
  });

  it("put each entity in its corresponding category array", () => {
    const testFilename = "testeroo.txt";
    const anatomyValue = testEntityData.ANATOMY[0];
    const medicationValue = testEntityData.MEDICATION[0];

    const res = generateMeta(testEntityData, testTextFrequency, testFilename);

    expect.assertions(3);
    expect(res.Attributes.ANATOMY).toContain(anatomyValue);
    expect(res.Attributes.MEDICATION).toContain(medicationValue);
    expect(res.Attributes.MEDICATION).not.toContain(anatomyValue);
  });

  it("empty metadata fields are represented as an empty array []", () => {
    const testFilename = "testeroo.txt";

    const res = generateMeta(testEntityData, testTextFrequency, testFilename);

    expect.assertions(1);
    expect(JSON.stringify(res.Attributes.TIME_EXPRESSION)).toMatch("");
  });

  it("take at most ELIMIT number of entities", () => {
    const testFilename = "testeroo.txt";
    const res = generateMeta(testEntityData, testTextFrequency, testFilename);
    let highestEntityCounter = 0;

    for (const cat of CATEGORIES) {
      let topEntities = res.Attributes[cat]
        .sort((a, b) => b[1] - a[1])
        .slice(0, ELIMIT);

      if (topEntities.length > highestEntityCounter) {
        highestEntityCounter = topEntities.length;
      }
    }

    expect.assertions(1);
    expect(highestEntityCounter <= ELIMIT).toBeTruthy();
  });

  it("order entities in each category, based on highest frequency", () => {
    const testFilename = "testeroo.txt";
    const res = generateMeta(testEntityData, testTextFrequency, testFilename);

    expect(JSON.stringify(res)).toMatch(JSON.stringify(testAttributes));
  });
});
