/**
 * RMO-META-UTIL for DIGITAL HUMAN
 *
 * This script takes a .txt file from the Resident Medical Officer handbook as input,
 * calls Comprehend Medical to detect medical entities in the document,
 * then creates attributes for Kendra searches.
 *
 * @author Ben Henshall / wsk9531@autuni.ac.nz
 */

const { argv } = require("process");
const {
  openDocument,
  splitDocument,
  detectEntities,
  processEntities,
  generateMeta,
  printAttributes,
} = require("./meta-util.js");

/**
 * The script takes ONE input parameter: the filename relative to local directory
 * script output: JSON to stdout
 * argv0 = node executable,
 * argv1 = entry point (index.js)
 * argv2+ = parameters
 */
async function main() {
  if (argv.length !== 3) {
    console.log("Usage: ", argv[0], argv[1], "< filename >");
    process.exitCode = 1;
  } else {
    const documentFilename = argv[2];
    const document = openDocument(documentFilename);
    const textBlocks = splitDocument(document);
    const responses = await detectEntities(textBlocks);
    const { entityData, textFrequency } = processEntities(responses);
    const attributes = generateMeta(entityData, textFrequency, documentFilename);
    printAttributes(attributes);
  }
}

main();
