// Constants

// Max number of strings in a STRING_LIST type attribute allowed by Kendra
const ELIMIT = 10;
// Minimum value  for the confidence score of a recognised entity returned by Comprehend Medical
const MIN_SCORE = 0.97;
// Comprehend Medical realtime analysis limit is 20000 chars
const COMPREHEND_MEDICAL_TEXT_SIZE = 19000;

// List of Comprehend Medical entity recognition categories
const CATEGORIES = [
  "ANATOMY",
  "MEDICAL_CONDITION",
  "MEDICATION",
  "PROTECTED_HEALTH_INFORMATION",
  "TEST_TREATMENT_PROCEDURE",
  "TIME_EXPRESSION",
];

module.exports = {
  ELIMIT,
  MIN_SCORE,
  COMPREHEND_MEDICAL_TEXT_SIZE,
  CATEGORIES,
};
