# rmo-meta-util

rmo-meta-util is a Node.js script and module for generating metadata from medical text.

It uses [AWS Comprehend Medical](https://aws.amazon.com/comprehend/medical/) to extract entities: `"ANATOMY", "MEDICAL_CONDITION", "MEDICATION", "PROTECTED_HEALTH_INFORMATION", "TEST_TREATMENT_PROCEDURE", "TIME_EXPRESSION"`.

It takes a specified number of entities over a relevance threshold and structures them into custom attributes for use in [AWS Kendra - Enterprise search engine](https://docs.aws.amazon.com/kendra/latest/dg/s3-metadata.html).

rmo-meta-util also assigns a \_source_uri attribute, which lets a user click on the URL in our app's chatbox, and open the latest version of that file.


## Table of Contents
- [Value](#value)
- [Installation](#installation)
- [Usage](#usage)
- [Importing](#importing)
- [Testing](#testing)
- [Opportunities](#opportunities)
- [Example Output](#example-output)
- [Contribution](#contribution)
- [Copyright](#copyright)

------------------
![aws-diagram](https://bitbucket.org/digital-human/digital-human-backend/raw/440f0f35a39b81e3349a606e90f126b612b63cd1/digital-human-comprehendmedical/images/rmo-diagram.PNG){width=250 height=250}
------------------
## Value

Kendra is an intelligent search engine, that uses a constantly evolving Machine-Learning model to provide the most relevant answers. Kendra is going to be most accurate with many users making many queries.

**What can we do to improve search when we don’t have either history or large counts of user interaction?**

Automatically generate metadata using ML to enrich the content and make it easier to search and discover!

Benefits:

1. Provides semi-structured data that improves Kendra’s search accuracy from the beginning.
2. Built in a modular way, such that the meta-util is easily integrated into other applications

## Installation

Currently, rmo-metal-util runs from the shell. Preferably on AWS Cloud Shell within the same region as the S3 Bucket and Kendra instance.

Clone the source code from the digital-human-backend repo. Follow any prompts.

```bash
$ git clone https://username@bitbucket.org/digital-human/digital-human-backend.git
```

Copy rmo-meta-util source files to ~/rmo-meta-util

```bash
$ cp digital-human-backend/digital-human-comprehendmedical/ rmo-meta-util -r
```

Move into the directory and install node packages

```bash
cd rmo-meta-util && npm install
```

## Usage

To run the script:

```bash
/rmo-meta-util/ $ node index.js <filename>
```

**This isn't very useful on its own.**

Instead, copy the RMO handbook files to the working directory

```bash
/rmo-meta-util/ $ aws s3 cp s3://<REPLACE-WITH-NAME-OF-YOUR-S3-BUCKET>/Data/ Data/  --recursive
```

Run `rmo.sh`, which calls the script as it loops over all files in `Data`.
This pipes the metadata into the corresponding `/Meta/Data/filename.fileextension.metadata.json` file.

```bash
/rmo-meta-util/ $ rmo.sh
```

Then upload the metadata back to the S3 bucket that Kendra is using as a Search Index.

```bash
/rmo-meta-util/ $ aws s3 cp Meta/ s3://<REPLACE-WITH-NAME-OF-YOUR-S3-BUCKET>/Meta/ --recursive
```

## Importing

The rmo-meta-util may be used in any application where medical metadata is useful!

Import any (or all) of the functions into your .js file:

```javascript
// import
const util = require("./meta-util.js");

// invoke
const x = util.generateMeta(...);
```

**OR** in a more verbose fashion...

```javascript
// import
const {
  openDocument,
  splitDocument,
  detectEntities,
  processEntities,
  generateMeta,
  printAttributes,
} = require("./meta-util.js");
```

## Testing

Unit tests are defined in:
`digital-human-backend/digital-human-comprehendmedical/rmo-meta-util/__tests__ ` and use the Jest framework.

To run tests, clone the repo to your local machine, navigate to the rmo-meta-util folder and run `npm test`

```bash
git clone https://username@bitbucket.org/digital-human/digital-human-backend.git

cd digital-human-backend/digital-human-comprehendmedical/rmo-meta-util

npm install

npm test
```

## Opportunities

This program was intended to further the overall goal of the Digital Human project: explore how innovative AI & Cognitive services could be used to enhance clinician workflows for Resident Medical Officers.

**There are many opportunities for improvement.**

1. This script could be re-written as an [AWS Lambda function](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html).

**Trigger-based behaviours could then be possible.**

E.g. When a page is uploaded to the RMO Handbook S3 bucket, metadata could be generated, saved, and synced up with Kendra automatically.

2. Developing a frontend for RMO handbook maintainers

**This would reduce the dependency maintainers currently have on developers to push a yearly update.** It would also reduce the need for print-outs to be used for pages with rapidly developing information, such as COVID-19 guidelines.

**Combined, the two would provide an automatic content ingestion pipeline.**
The information would be kept current, and resident medical officers would not have to juggle paper printouts and the RMO app.

3. Add query filtering to the digital-human frontend application's Kendra requests

**The value is being able to create faceted search suggestions, use Boolean logic to filter out documents that don't match specific criteria or filter out specific document attributes - such as Protected Health Information (PHI).**

As of writing (19/10/2021), [filtering and faceting](https://docs.aws.amazon.com/kendra/latest/dg/filtering.html) has not been implemented into the mobile application. Our team instead decided to focus on more visual, RMO-requested features, like bookmarks and checklists.

While Kendra is an intelligent search engine and will use metadata to determine relevance, the possibilities go much further.

**In short, this allows for searches to drill much deeper into a large corpus of documents than previous versions of the app, which use fuzzy-match keyword search.**

## Example Output 
(from Wikipedia's [Epinephrine](https://en.wikipedia.org/wiki/Epinephrine)

```json
{
  "Attributes": {
    "ANATOMY": [
      "heart",
      "adrenal medulla",
      "adrenal glands",
      "adrenal cortex",
      "lungs",
      "lung"
    ],
    "MEDICAL_CONDITION": [
      "asthma",
      "tremor",
      "anxiety",
      "hypoglycemia",
      "shakiness",
      "sweating",
      "hypoglycaemia",
      "neuropathy"
    ],
    "MEDICATION": [
      "adrenaline",
      "epinephrine",
      "noradrenaline",
      "Dopamine",
      "Norepinephrine",
      "glucagon",
      "Droxidopa",
      "L-Phenylalanine",
      "N-Methylphenethylamine",
      "p-Octopamine"
    ],
    "PROTECTED_HEALTH_INFORMATION": [
      "ed.",
      "Wikidata",
      "2007",
      "Abel",
      "2006",
      "2008",
      "2009",
      "January 1984",
      "1977",
      "1996"
    ],
    "TEST_TREATMENT_PROCEDURE": ["adrenocorticotropic hormone"],
    "TIME_EXPRESSION": [],
    "_source_uri": "https://digital-human-rmo-handbook.s3.ap-southeast-2.amazonaws.com/Data/testeroo.txt"
  }
}
```

## Contribution

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Copyright

All rights reserved.
